import 'package:flutter_2/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_2/register.dart';
import 'package:flutter_2/login.dart';
import 'package:flutter_2/bottom_nav_bar.dart';
import 'package:flutter_2/home.dart';
import 'package:flutter_2/feed.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MySplashScreen(),
    routes: {
      'register': (context) => MyRegister(),
      'splashscreen': (context) => MySplashScreen(),
      'login': (context) => MyLogin(),
      'navigationbar': (context) => MyNavigationBar(),
      'home': (context) => MyHome(),
    },
  ));
}
