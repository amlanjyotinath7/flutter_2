import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_2/constant.dart';
import 'package:flutter_2/data.dart';

class MyHome extends StatefulWidget {
  const MyHome({super.key});

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  Map<String, dynamic> color = {
    "Initiation Quest": primaryColor,
    "Daily Quest": primaryColor,
    "Cause Quest": primaryColor,
    "Attend a meeting": newColor,
    "Personal Campaign": secondaryColor,
    "Donation Campaign": secondaryColor,
  };

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height * 0.2;
    final width = MediaQuery.of(context).size.height * 0.2;
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFFFFFFF),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [],
            ),
            const Padding(
              padding:
                  EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
              child: Text(
                "Welcome\nJohn",
                style: TextStyle(fontSize: 20, color: primaryColor),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    ...hometype.map(
                      (e) => Card(
                        color: color[e['card']],
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: SizedBox(
                          height: height,
                          width: width,
                          child: Center(
                            child: Text(
                              e['card'],
                              style: const TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            const Padding(
              padding:
                  EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
              child: Text(
                "Quest",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, color: primaryColor),
              ),
            ),
            Column(
              children: [
                ...home.map(
                  (e) => Card(
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return const AlertDialog(
                                  title:
                                      Center(child: Text('Write an Artical')),
                                );
                              });
                          // showModalBottomSheet(
                          //     context: context,
                          //     builder: (context) {
                          //       return SizedBox(
                          //         height:
                          //             MediaQuery.of(context).size.height * 0.2,
                          //         child: const Padding(
                          //           padding: EdgeInsets.all(8.0),
                          //           child: Card(
                          //             child: Text("hello"),
                          //           ),
                          //         ),
                          //       );
                          //     });
                        },
                        title: Text(e['Quest']),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
