import 'package:flutter_2/constant.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Notifications extends StatelessWidget {
  const Notifications({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Notification'),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
    );
  }
}
