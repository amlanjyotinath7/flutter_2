import 'package:flutter_2/feed.dart';
import 'package:flutter_2/constant.dart';
import 'package:flutter_2/data.dart';
import 'package:flutter/material.dart';

class LiveFeed extends StatefulWidget {
  const LiveFeed({super.key});

  @override
  State<LiveFeed> createState() => _LiveFeedState();
}

class _LiveFeedState extends State<LiveFeed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Live Feed'),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: ListView(
        children: [
          ...questType.map((e) => Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  child: ListTile(
                    title: Text(e['card']),
                    trailing: const CircleAvatar(
                      backgroundColor: primaryColor,
                      child: Icon(Icons.person),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Feed(),
                          ));
                    },
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
