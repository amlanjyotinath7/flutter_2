import 'package:flutter_2/constant.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text("Account"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
        child: ListView(
          children: [
            const CircleAvatar(
              foregroundColor: primaryColor,
              radius: 40,
              backgroundColor: Colors.white,
              child: Icon(
                Icons.person,
                size: 50,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextFormField(
              initialValue: "Mriganka",
              readOnly: true,
              decoration: const InputDecoration(
                prefixIcon: Icon(
                  Icons.person,
                  color: primaryColor,
                ),
              ),
            ),
            TextFormField(
              initialValue: "mrihanka9@gmail.com",
              readOnly: true,
              decoration: const InputDecoration(
                prefixIcon: Icon(
                  Icons.mail,
                  color: primaryColor,
                ),
              ),
            ),
            TextFormField(
              readOnly: true,
              initialValue: "6001729089",
              decoration: const InputDecoration(
                prefixIcon: Icon(
                  Icons.phone,
                  color: primaryColor,
                ),
              ),
            ),
            TextFormField(
              initialValue: "17/01/1998",
              readOnly: true,
              decoration: const InputDecoration(
                prefixIcon: Icon(
                  Icons.date_range,
                  color: primaryColor,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: ElevatedButton.icon(
                  onPressed: () {},
                  icon: const Icon(Icons.arrow_forward),
                  label: const Text('Edit')),
            )
          ],
        ),
      ),
    );
  }
}
