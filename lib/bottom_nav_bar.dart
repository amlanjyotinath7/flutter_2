import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_2/constant.dart';
import 'package:flutter_2/home.dart';
import 'package:flutter_2/feed.dart';
import 'package:flutter_2/live_feed.dart';
import 'package:flutter_2/notifications.dart';
import 'package:flutter_2/profile.dart';
import 'package:flutter_2/quest_hub.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyNavigationBar(),
    );
  }
}

class MyNavigationBar extends StatefulWidget {
  MyNavigationBar({Key? key}) : super(key: key);

  @override
  _MyNavigationBarState createState() => _MyNavigationBarState();
}

class _MyNavigationBarState extends State<MyNavigationBar> {
  int _selectedIndex = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    Text('Home Page',
        style: TextStyle(
          fontSize: 35,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        )),
    Text('Quest Hub',
        style: TextStyle(
          fontSize: 35,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        )),
    Text('Live Feed',
        style: TextStyle(
          fontSize: 35,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        )),
    Text('Profile',
        style: TextStyle(
          fontSize: 35,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        )),
    Text('Notification',
        style: TextStyle(
          fontSize: 35,
          fontWeight: FontWeight.bold,
          color: newColor,
        )),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Map<int, dynamic> page = {
    0: MyHome(),
    1: const Quest(),
    2: const LiveFeed(),
    3: const Profile(),
    4: const Notifications(),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: page[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home, color: Color(0xFFFFD95A)),
                label: ('Home'),
                backgroundColor: Color(0xFF0F3B60)),
            BottomNavigationBarItem(
                icon: Icon(Icons.message, color: Color(0xFFFFD95A)),
                label: ('Quest Hub'),
                backgroundColor: Color(0xFF0F3B60)),
            BottomNavigationBarItem(
                icon: Icon(Icons.feed, color: Color(0xFFFFD95A)),
                label: ('Live Feed'),
                backgroundColor: Color(0xFF0F3B60)),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_2_rounded, color: Color(0xFFFFD95A)),
                label: ('Profile'),
                backgroundColor: Color(0xFF0F3B60)),
            BottomNavigationBarItem(
                icon: Icon(Icons.notifications, color: Color(0xFFFFD95A)),
                label: ('Notification'),
                backgroundColor: Color(0xFF0F3B60)),
          ],
          type: BottomNavigationBarType.shifting,
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.black,
          iconSize: 40,
          onTap: _onItemTapped,
          elevation: 5),
    );
  }
}
