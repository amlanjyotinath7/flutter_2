import 'package:flutter/material.dart';
import 'package:flutter_2/constant.dart';
import 'package:flutter_2/data.dart';
import 'package:percent_indicator/percent_indicator.dart';

class Quest extends StatefulWidget {
  const Quest({super.key});

  @override
  State<Quest> createState() => _QuestState();
}

class _QuestState extends State<Quest> with TickerProviderStateMixin {
  double slidevalue = 0;
  int pageIndex = 0;
  late AnimationController _animationController;
  Map<String, dynamic> color = {
    "Daily Quest": secondaryColor,
    "Other Quest": primaryColor,
    "Active Quest": secondaryColor
  };
  Map<String, dynamic> textcolor = {
    "Daily Quest": Colors.black,
    "Other Quest": Colors.white,
  };
  String startdate =
      "${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}";

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 500),
        upperBound: 6);

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height * 0.1;
    final width = MediaQuery.of(context).size.height * 0.2;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quest Hub'),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.12,
                left: 10,
                right: 10),
            child: ListView(
              children: [
                ...quest[pageIndex].map(
                  (e) => AnimatedBuilder(
                    animation: _animationController,
                    child: GestureDetector(
                      onTap: () {
                        showPopUp(context);
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        elevation: 4,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${e['a']} in progress(50%)",
                                style: const TextStyle(
                                    fontSize: 15, color: primaryColor),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Started On $startdate",
                                    style: const TextStyle(
                                        fontSize: 10, color: primaryColor),
                                  ),
                                  CircularPercentIndicator(
                                    radius: 15.0,
                                    lineWidth: 3.0,
                                    percent: 0.5,
                                    center: const Text(
                                      '50%',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    progressColor: primaryColor,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    builder: (context, child) => Padding(
                      padding: EdgeInsets.only(
                          top: 600 - _animationController.value * 100),
                      child: child,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...questType.map(
                    (e) => GestureDetector(
                      onTap: () {
                        setState(() {
                          pageIndex = questType.indexOf(e);
                        });
                      },
                      child: Card(
                        color: color[e['card']],
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: SizedBox(
                          height: height,
                          width: width,
                          child: Center(
                            child: Text(
                              e['card'],
                              style: TextStyle(color: textcolor[e['card']]),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<dynamic> showPopUp(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Center(
                child: Column(
              children: [
                const Text(
                  'Write an Artical',
                  style: TextStyle(fontSize: 15),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Description: An article writing is a kind of writing that is written to reach a massive audience with the help of the press. In the case of article writing, the press refers to publishing houses of newspapers, magazines, journals, etc.',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                GestureDetector(
                  onTap: () {},
                  child: const Text('Accept Quest',
                      style: TextStyle(
                        fontSize: 15,
                      )),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(),
                const SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Cancel Quest',
                    style: TextStyle(color: Colors.red, fontSize: 15),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(),
                const SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {},
                  child: const Text('Invite Others',
                      style: TextStyle(fontSize: 15)),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            )),
          );
        });
  }
}
